from higgs_dna.tools.chained_quantile import ChainedQuantileRegression
from higgs_dna.tools.diphoton_mva import calculate_diphoton_mva
from higgs_dna.tools.xgb_loader import load_bdt
from higgs_dna.tools.photonid_mva import calculate_photonid_mva, load_photonid_mva
from higgs_dna.tools.SC_eta import add_photon_SC_eta
from higgs_dna.tools.EELeak_region import veto_EEleak_flag
from higgs_dna.selections.photon_selections import photon_preselection
from higgs_dna.selections.lepton_selections import select_electrons, select_muons
from higgs_dna.selections.jet_selections import select_jets, jetvetomap
from higgs_dna.selections.object_selections import delta_r_match_mask, delta_r_mask
from higgs_dna.utils.dumping_utils import diphoton_ak_array, ditau_ak_array, dump_ak_array
from higgs_dna.selections.lumi_selections import select_lumis
from higgs_dna.utils.misc_utils import choose_jet
from higgs_dna.utils.kinematic_utilities import calculate_delta_phi, calculate_transverse_mass, calculate_transverse_momentum

from higgs_dna.tools.mass_decorrelator import decorrelate_mass_resolution

# from higgs_dna.utils.dumping_utils import diphoton_list_to_pandas, dump_pandas
from higgs_dna.metaconditions import photon_id_mva_weights
from higgs_dna.metaconditions import diphoton as diphoton_mva_dir
from higgs_dna.systematics import object_systematics as available_object_systematics
from higgs_dna.systematics import object_corrections as available_object_corrections
from higgs_dna.systematics import weight_systematics as available_weight_systematics
from higgs_dna.systematics import weight_corrections as available_weight_corrections

import functools
import operator
import os
import warnings
from typing import Any, Dict, List, Optional
import awkward
import numpy
import vector
from coffea import processor
from coffea.analysis_tools import Weights
from copy import deepcopy

import logging

logger = logging.getLogger(__name__)

vector.register_awkward()


class HtautauBaseProcessor(processor.ProcessorABC):
    def __init__(
        self,
        metaconditions: Dict[str, Any],
        systematics: Optional[Dict[str, List[str]]],
        corrections: Optional[Dict[str, List[str]]],
        apply_trigger: bool,
        output_location: Optional[str],
        taggers: Optional[List[Any]],
        trigger_group: str,
        analysis: str,
    ) -> None:
        self.meta = metaconditions
        self.systematics = systematics if systematics is not None else {}
        self.corrections = corrections if corrections is not None else {}
        self.output_location = output_location
        self.trigger_group = trigger_group
        self.analysis = analysis
        self.apply_trigger = apply_trigger

        self.min_pt_muon = 20.0
        self.min_pt_lead_muon = 25.0

        self.postfixes = {"obj_1": "1", "obj_2": "2"}

    def process_extra(self, events: awkward.Array) -> awkward.Array:
        raise NotImplementedError

    def select_muons(self, muons: awkward.highlevel.Array, muon_pt_threshold: float, muon_max_eta: float) -> awkward.highlevel.Array:
        """
        Selects muons from the input `muons` array based on specified criteria.

        Args:
            muons (awkward.highlevel.Array): The input array containing muon objects.
            muon_pt_threshold (float): The minimum transverse momentum threshold for muon selection.
            muon_max_eta (float): The maximum absolute rapidity for muon selection.

        Returns:
            awkward.highlevel.Array: The selected muons satisfying the given criteria.

        Example:
            muons = ...  # Initialize your awkward.highlevel.Array of muons
            selected_muons = select_muons(muons, muon_pt_threshold=10.0, muon_max_eta=2.4)

        Note:
            - The provided `muons` should be an `awkward.highlevel.Array` containing muon objects.
            - The function applies selection criteria on the `muons` array based on the following conditions:
                - Transverse momentum (pt) greater than `muon_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`muon_max_eta`, +`muon_max_eta`).
                - Medium identification (mediumId) requirement.
                - Particle flow relative isolation (pfRelIso04_all) less than 0.3.
                - Transverse impact parameter (dz) less than 0.2.
                - Longitudinal impact parameter (dxy) less than 0.045.
            - The function returns the muons that satisfy all the specified selection criteria.

        """
        pt_cut = muons.pt > muon_pt_threshold
        eta_cut = abs(muons.eta) < abs(muon_max_eta)
        id_cut = muons.mediumId
        iso_cut = muons.pfRelIso04_all < 0.3
        dz_cut = abs(muons.dz) < 0.2
        dxy_cut = abs(muons.dxy) < 0.045
        return muons[pt_cut & eta_cut & id_cut & iso_cut & dz_cut & dxy_cut]

    def select_electrons(self, electrons: awkward.highlevel.Array, electron_pt_threshold: float, electron_max_eta: float, electron_id : str) -> awkward.highlevel.Array:
        """
        Selects electrons from the input `electrons` array based on specified criteria.

        Args:
            electrons (awkward.highlevel.Array): The input array containing electron objects.
            electron_pt_threshold (float): The minimum transverse momentum threshold for electron selection.
            electron_max_eta (float): The maximum absolute rapidity for electron selection.
            electron_id (str): The name of the electron ID discriminator to use.

        Returns:
            awkward.highlevel.Array: The selected electrons satisfying the given criteria.

        Example:
            electrons = ...  # Initialize your awkward.highlevel.Array of electrons
            selected_electrons = select_electrons(electrons, electron_pt_threshold=10.0, electron_max_eta=2.5, electron_id='mvaNoIso_WP90')
            Recommended IDs are mvaNoIso_WP90 for Run-3 or mvaFall17V1Iso_WP90 for Run-2
        Note:
            - The provided `electrons` should be an `awkward.highlevel.Array` containing electron objects.
            - The function applies selection criteria on the `electrons` array based on the following conditions:
                - Transverse momentum (pt) greater than `electron_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`electron_max_eta`, +`electron_max_eta`).
                - identification requirement -> electron should pass the ID discriminator `electron_id`.
                - Particle flow relative isolation (pfRelIso04_all) less than 0.3.
                - Transverse impact parameter (dz) less than 0.2.
                - Longitudinal impact parameter (dxy) less than 0.045.
            - The function returns the electrons that satisfy all the specified selection criteria.

        """

        pt_cut = electrons.pt > electron_pt_threshold
        eta_cut = abs(electrons.eta) < abs(electron_max_eta)
        id_cut = electrons[electron_id]
        iso_cut = electrons.miniPFRelIso_all < 0.3
        dz_cut = abs(electrons.dz) < 0.2
        dxy_cut = abs(electrons.dxy) < 0.045
        return electrons[pt_cut & eta_cut & id_cut & iso_cut & dz_cut & dxy_cut]

    def select_taus(self, taus: awkward.highlevel.Array, tau_pt_threshold: float, tau_max_eta: float, tau_id : str) -> awkward.highlevel.Array:
        """
        Selects taus from the input `taus` array based on specified criteria.

        Args:
            taus (awkward.highlevel.Array): The input array containing tau objects.
            tau_pt_threshold (float): The minimum transverse momentum threshold for tau selection.
            tau_max_eta (float): The maximum absolute rapidity for tau selection.
            tau_id (str): The name of the tau ID discriminator to use.

        Returns:
            awkward.highlevel.Array: The selected taus satisfying the given criteria.

        Example:
            taus = ...  # Initialize your awkward.highlevel.Array of taus
            selected_taus = select_taus(taus, tau_pt_threshold=18.0, tau_max_eta=2.3, tau_id='DeepTau2018v2p5')

        Note:
            - The provided `taus` should be an `awkward.highlevel.Array` containing tau objects.
            - The function applies selection criteria on the `taus` array based on the following conditions:
                - Transverse momentum (pt) greater than `tau_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`tau_max_eta`, +`tau_max_eta`).
                - identification requirement -> tau should pass the loosest working points of the ID discriminator `tau_id`.
                - Transverse impact parameter (dz) less than 0.2.
            - The function returns the taus that satisfy all the specified selection criteria.

        """
        pt_cut = taus.pt > tau_pt_threshold
        eta_cut = abs(taus.eta) < abs(tau_max_eta)
        # apply the loosest working points for jet and muon discriminators, and the second loosest electron discriminator
        # as we are using the DeepTau ID we also veto dm 5 and 6, when the code is updated to use particleNet we may want to remove this requirement
        id_cut = (taus[f"id{tau_id}VSjet"] > 0) & (taus[f"id{tau_id}VSmu"] > 0) & (taus[f"id{tau_id}VSe"] > 1) & (taus.decayMode != 5) & (taus.decayMode != 6)
        dz_cut = abs(taus.dz) < 0.2
        return taus[pt_cut & eta_cut & dz_cut & id_cut]

    def select_jets(self, jets: awkward.highlevel.Array, jet_pt_threshold: float, jet_max_eta: float) -> awkward.highlevel.Array:
        """
        Selects jets from the input `jets` array based on specified criteria.

        Args:
            jets (awkward.highlevel.Array): The input array containing jet objects.
            jet_pt_threshold (float): The minimum transverse momentum threshold for jet selection.
            jet_max_eta (float): The maximum absolute rapidity for jet selection.

        Returns:
            awkward.highlevel.Array: The selected jets satisfying the given criteria.

        Example:
            jets = ...  # Initialize your awkward.highlevel.Array of jets
            selected_jets = select_ejst(jets, jet_pt_threshold=30.0, jet_max_eta=4.7)

        Note:
            - The provided `jets` should be an `awkward.highlevel.Array` containing jet objects.
            - The function applies selection criteria on the `jets` array based on the following conditions:
                - Transverse momentum (pt) greater than `jet_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`jet_max_eta`, +`jet_max_eta`).
            - The function returns the jets that satisfy all the specified selection criteria.

        """
        pt_cut = jets.pt > jet_pt_threshold
        eta_cut = abs(jets.eta) < abs(jet_max_eta)
        # apply the tight jet ID
        id_cut = (jets.jetId & 3) != 0
        return jets[pt_cut & eta_cut & id_cut]

    def add_pair_quantities(self, pairs: awkward.Array) -> awkward.Array:
        """
        Adds quantities to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.

        Returns:
            awkward.Array: The modified pairs with additional quantities.

        - The pairs are transformed into candidates with four momenta and additional properties.

        The modified pairs are returned as an Awkward Array with the name 'PtEtaPhiMCandidate'.
        """

        logger.debug("Adding ditau pair quantities")
        # turn the pairs into candidates with four momenta and such
        pair_4mom = pairs["obj_1"] + pairs["obj_2"]
        pairs["pt"] = pair_4mom.pt
        pairs["eta"] = pair_4mom.eta
        pairs["phi"] = pair_4mom.phi
        pairs["mass"] = pair_4mom.mass
        pairs["charge"] = pair_4mom.charge
        pairs["os"] = pair_4mom.charge == 0
        pairs["dR"] = pairs["obj_1"].delta_r(pairs["obj_2"])
        pairs = awkward.with_name(pairs, "PtEtaPhiMCandidate")
        pairs['dphi'] = calculate_delta_phi(pairs["obj_1"].phi,pairs["obj_2"].phi)

        return pairs

    def add_met_quantities(self, pairs: awkward.Array, met: awkward.Array) -> awkward.Array:
        """
        Adds MET-related quantities to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.
            met (awkward.Array): An Awkward Array containing the MET.

        Returns:
            awkward.Array: The modified pairs with additional MET-related quantities.
        """

        logger.debug("Adding MET quantities")

        # met related quantities
        pairs["met"] = met.pt
        pairs["met_dphi_1"] = calculate_delta_phi(pairs["obj_1"].phi,met.phi)
        pairs["met_dphi_2"] = calculate_delta_phi(pairs["obj_2"].phi,met.phi)
        pairs['mt_1'] = calculate_transverse_mass(pairs["obj_1"],met)
        pairs['mt_2'] = calculate_transverse_mass(pairs["obj_2"],met)
        pairs['mt_lep'] = calculate_transverse_mass(pairs["obj_1"],pairs["obj_2"])
        pairs['mt_tot'] = numpy.sqrt(pairs.mt_1**2 + pairs.mt_2**2 + pairs.mt_lep**2)
        pairs['pt_tt'] = calculate_transverse_momentum(pairs,met)

        return pairs

    def add_jet_quantities(self, pairs: awkward.Array, jets: awkward.Array) -> awkward.Array:
        """
        Adds jet-related quantities to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.
            jets (awkward.Array): An Awkward Array containing the jets.

        Returns:
            awkward.Array: The modified pairs with additional jet-related quantities.
        """

        logger.debug("Adding jet quantities")

        # ensure jets are ordered by pT
        jets = jets[awkward.argsort(jets.pt, ascending=False)]

        # set charge of all jets to 0 as it is not defined
        jets["charge"] = awkward.zeros_like(jets.pt)

        default_value = -9999.

        lead_jet = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 0, default_value),
                "eta": choose_jet(jets.eta, 0, default_value),
                "phi": choose_jet(jets.phi, 0, default_value),
                "mass": choose_jet(jets.mass, 0, default_value),
                "charge":choose_jet(jets.charge, 0, default_value),
            }
        )
        lead_jet = awkward.with_name(lead_jet, "PtEtaPhiMCandidate")

        sublead_jet = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 1, default_value),
                "eta": choose_jet(jets.eta, 1, default_value),
                "phi": choose_jet(jets.phi, 1, default_value),
                "mass": choose_jet(jets.mass, 1, default_value),
                "charge":choose_jet(jets.charge, 1, default_value),
            }
        )
        sublead_jet = awkward.with_name(sublead_jet, "PtEtaPhiMCandidate")

        pairs['jet_1'] = lead_jet
        pairs['jet_2'] = sublead_jet

        dijet = lead_jet + sublead_jet

        pairs["jpt_1"] = lead_jet.pt
        pairs["jeta_1"] = lead_jet.eta
        pairs["jphi_1"] = lead_jet.phi
        pairs["jpt_2"] = sublead_jet.pt
        pairs["jeta_2"] = sublead_jet.eta
        pairs["jphi_2"] = sublead_jet.phi
        pairs["n_jets"] = awkward.num(jets)
        pairs["dijetpt"] = awkward.where(pairs["n_jets"] >= 2, dijet.pt, default_value)
        pairs["mjj"] = awkward.where(pairs["n_jets"] >= 2, dijet.mass, default_value)
        pairs["jdeta"] = awkward.where(pairs["n_jets"] >= 2, numpy.abs(pairs["jeta_1"] - pairs["jeta_2"]), default_value)

        # signed jet delta phi, where sign is defined by eta ordering of the jets
        sjdphi = awkward.where(lead_jet.eta >= sublead_jet.eta, calculate_delta_phi(lead_jet.phi,sublead_jet.phi), calculate_delta_phi(sublead_jet.phi,lead_jet.phi))
        pairs["sjdphi"] = awkward.where(pairs["n_jets"] >= 2, sjdphi, default_value)

        return pairs

    def apply_met_filters(self, events: awkward.Array) -> awkward.Array:
        """
        Apply MET filters to the events and return the filtered events.

        Args:
            events (awkward.Array): An Awkward Array containing the events data.

        Returns:
            awkward.Array: The filtered events after applying MET filters.

        This function applies MET filters to the events based on a list of filters specified in the metaconditions.
        Note: The MET filters are applied using the logical AND operation on the individual filter flags.
        """

        logger.debug("Applying MET filters")
        # met filters
        met_filters = self.meta["MetFilters"][self.data_kind]
        filtered = functools.reduce(
            operator.and_,
            (events.Flag[metfilter.split("_")[-1]] for metfilter in met_filters),
        )
        return events[filtered]

    def get_trigger_OR_mask(self, objects : awkward.Array, trig_objects : awkward.Array, selections : str, dR : float) -> awkward.Array:
        """
        Get the trigger OR mask for objects based on a list of trigger selections and delta R matching.

        This function applies a set of trigger selections on the 'trig_objects' and returns a mask that
        represents the logical OR (disjunction) of those selections. It also performs delta R matching
        between the selected trigger objects and 'objects', returning the matched objects.

        Parameters:
            objects (awkward.Array): The array of objects for delta R matching.
            trig_objects (awkward.Array): The array of trigger objects for applying selections.
            selections (str): A string containing trigger selections separated by ':' for logical OR.
                              Individual selections are separated by ','. Supported comparisons are '==', '=', '>=',
                              '>', '<=', and '<'. Bitwise AND operation is supported with '&'.
                              Example: 'id==13,pt>30,filterBits&3'
            dR (float): The delta R threshold for matching objects.

        Returns:
            awkward.Array: An array with boolean values indicating if the objects passed the delta R matching and
                           trigger OR selection. The array contains 'True' for objects that have matching trigger
                           objects according to the specified selections and delta R threshold, and 'False' otherwise.

        Note:
            If no trigger selections are specified (or '1' or 'None' is provided), the function returns an array
            that is 'True' for non-None values (i.e., there is an offline object) and 'False' for None values.
        """

        # if no trigger selections were specified then the object is taken to have passed the selection
        allowed_operations = set(['==', '=', '>=', '>', '<=', '<', '&'])
        selections_check = selections.strip().lower()  # Convert to lowercase and remove leading/trailing spaces
        if selections_check == '1' or selections_check == 'none' or not any(op in selections for op in allowed_operations):
            # return an array that is false for none value and true for non-None values(i.e there is an offline object)
            return ~awkward.is_none(objects)

        masks_for_OR = []
        for sels in selections.split(':'):
            sels_vec = sels.split(',')
            AND_mask = trig_objects.filterBits > 0
            for sel in sels_vec:
                sel_no_spaces = sel.replace(' ','')
                if '==' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('==')[0]] == float(sel_no_spaces.split('==')[1]))
                elif '=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('=')[0]] == float(sel_no_spaces.split('=')[1]))
                elif '>=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('>=')[0]] >= float(sel_no_spaces.split('>=')[1]))
                elif '>' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('>')[0]] > float(sel_no_spaces.split('>')[1]))
                elif '<=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('<=')[0]] <= float(sel_no_spaces.split('<=')[1]))
                elif '<' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('<')[0]] > float(sel_no_spaces.split('<')[1]))
                elif '&' in sel_no_spaces:
                    AND_mask = AND_mask & ((trig_objects[sel_no_spaces.split('&')[0]] & int(sel_no_spaces.split('&')[1])) != 0)
            masks_for_OR.append(AND_mask)

        # Combine all masks with logical OR operation
        mask = awkward.any(masks_for_OR,axis=0)

        # passing trigger objects are selected by requiring them to pass the OR selection AND their is a corresponding offline object
        passing_trig_objects = trig_objects[mask & ~awkward.is_none(objects)]

        # Apply delta R matching and get the matched objects
        matched_objects = passing_trig_objects[delta_r_match_mask(passing_trig_objects, objects, dR)]

        # return an array that is false if there are no matched trigger objects or true if there are matches
        return awkward.num(matched_objects) > 0

    def apply_triggers(self, trig_objects: awkward.Array, pairs: awkward.Array, outname : str, leg1_selections: str = None, leg2_selections : str = None, leg3_selections : str = None, leg4_selections : str = None) -> awkward.Array:
        """
        Apply triggers to candidate pairs in the provided 'pairs' array based on trigger matching criteria.

        Parameters:
            trig_objects (awkward.Array): Awkward Array containing the trigger objects.
            pairs (awkward.Array): Awkward Array containing the candidate pairs.
            outname (str): Name of the output column that will be added to the 'pairs' array to indicate trigger matches.
            leg1_selections (str): Trigger matching criteria for the first leg (obj_1) of candidate pairs.
            leg2_selections (str): Trigger matching criteria for the second leg (obj_2) of candidate pairs.

        Returns:
            awkward.Array: Awkward Array containing the candidate pairs with an additional bool column indicating trigger matches.
        """

        # get trigger matched for first leg (obj_1)
        if leg1_selections:
            leg1_matches = self.get_trigger_OR_mask(pairs['obj_1'],trig_objects,leg1_selections,0.5)
        else:
            leg1_matches = True
        # get trigger matched for second leg (obj_2)
        if leg2_selections:
            leg2_matches = self.get_trigger_OR_mask(pairs['obj_2'],trig_objects,leg2_selections,0.5)
        else:
            leg2_matches = True
        # get trigger matched for third leg (leading jet)
        if leg3_selections:
            leg3_matches = self.get_trigger_OR_mask(pairs['jet_1'],trig_objects,leg3_selections,0.5)
            # default phi and eta of jets that don't exist should be -9999 so dR matching for trigger objects should
            # never pass anyway. But to be extra careful we set the matches to False if the leading jet does not exist (n_jets<1)
            leg3_matches = awkward.where(pairs["n_jets"] >= 1, leg3_matches, False)
        else:
            leg3_matches = True
        # get trigger matched for fourth leg (subleading jet)
        if leg4_selections:
            leg4_matches = self.get_trigger_OR_mask(pairs['jet_2'],trig_objects,leg4_selections,0.5)
            # default phi and eta of jets that don't exist should be -9999 so dR matching for trigger objects should
            # never pass anyway. But to be extra careful we set the matches to False if the subleading jet does not exist (n_jets<2)
            leg4_matches = awkward.where(pairs["n_jets"] >= 2, leg4_matches, False)
        else:
            leg4_matches = True

        # now add a bool as a variable to indicate if the trigger passed or not (i.e both legs pass)
        pairs[outname] = leg1_matches & leg2_matches & leg3_matches & leg4_matches

        return pairs

    def rename_and_drop(self, array: awkward.Array, channel: str) -> awkward.Array:
        """
        Renames and drops columns in the provided `awkward.Array` based on a predefined mapping.

        Args:
            array (awkward.Array): The input array containing columns to be renamed and dropped.
            channel (str): A string that I defines the ditau channel ('mm','ee','et','mt','em','tt') which determines which quantities are stored

        Returns:
            awkward.Array: The modified array with columns renamed and dropped based on the mapping.

        Example:
            array = ...  # Initialize your awkward.Array
            modified_array = rename_and_drop(array,'tt')

        Note:
            - The provided `array` should be an `awkward.Array`.
            - The function uses the `awkward.zip` function to rename and drop columns based on the mapping.
            - Columns with `None` values in the mapping will retain their original names.
            - Columns not present in the mapping will be dropped from the resulting array.
        """

        # TODO: map is predefined for now, but could be passed as an argument instead
        map_dict = {
            "pt_1" : None,
            "pt_2" : None,
            "eta_1" : None,
            "eta_2" : None,
            "phi_1": None,
            "phi_2": None,
            "dR" : None,
            "q_1" : None,
            "q_2" : None,
            "pt_tt" : None,
            "mt_1" : None,
            "mt_2" : None,
            "mt_lep" : None,
            "mt_tot" : None,
            "os" : None,
            "met_dphi_1" : None,
            "met_dphi_2" : None,
            "dphi" : None,
            "trg_singlemuon" : None,
            "trg_singleelectron" : None,
            "trg_singleelectron_2" : None,
            "trg_singlemuon_2" : None,
            "trg_doubletau" : None,
            "trg_doubletauandjet": "",
            "trg_doubletauandjet_2": "",
            "pfRelIso04_all_1" : "iso_1",
            "pfRelIso04_all_2" : "iso_2",
            "miniPFRelIso_all_1": "iso_1",
            "miniPFRelIso_all_2": "iso_2",
            "idDeepTau2018v2p5VSjet_1" : "",
            "idDeepTau2018v2p5VSjet_2" : "",
            "idDeepTau2018v2p5VSmu_1" : "",
            "idDeepTau2018v2p5VSmu_2" : "",
            "idDeepTau2018v2p5VSe_1" : "",
            "idDeepTau2018v2p5VSe_2" : "",
            "mass" : "m_vis",
            "pt" : "pt_vis",
            "n_jets": "",
            "n_bjets": "",
            "mjj": "",
            "jdeta": "",
            "sjdphi": "",
            "dijetpt": "",
            "jpt_1": "",
            "jpt_2": "",
            "jeta_1": "",
            "jeta_2": "",
            "jphi_1": "",
            "jphi_2": "",
        }

        # Drop columns not in the map and rename columns based on the map
        array_renamed = awkward.zip(
            {new_name if not (new_name == '' or new_name is None) else old_name: array[old_name]
             for old_name, new_name in map_dict.items() if old_name in array.fields}
        )

        return array_renamed

    def process(self, events: awkward.Array) -> Dict[Any, Any]:
        # data or monte carlo?
        self.data_kind = "mc" if hasattr(events, "GenPart") else "data"

        # metadata array to append to higgsdna output
        metadata = {}

        if 'Channels' not in self.meta:
            channels = "all"
        else:
            channels = self.meta['Channels']
        if channels == "all":
            channels = ['mm','ee','em','et','mt','tt']
        else:
            channels = channels.split(',')
        logger.debug(f"Processing channels: {channels}")

        # apply met filters
        events = self.apply_met_filters(events)

        # here we start recording possible coffea accumulators
        # most likely histograms, could be counters, arrays, ...
        histos_etc = {}

        original_muons = events.Muon
        original_electrons = events.Electron
        original_taus = events.Tau
        # add default objects as well as objects with systematic shifts applied to dictionary
        objs_dct = {}
        # no systematic variations added so far
        objs_dct["nominal"] = (original_electrons, original_muons, original_taus)

        original_jets = events.Jet
        jets_dct = {}
        jets_dct["nominal"] = original_jets

        original_met = events[self.meta['MET_Type']]
        met_dct = {}
        met_dct["nominal"] = original_met

        for variation, objs in objs_dct.items():
            logger.debug(f"Variation: {variation}")
            electrons,muons,taus = objs

            # TODO: when it actually comes to implementing systematic variations e.g energy scales shifts this code will not be optimal since it will perform the object preselections again for all 3 objects e.g if electron energy is shifted muons and taus will be preselected again - this can be improved
            # object preselection
            # first we select muons using loose pt and isolation cuts so that they can be used in all channels and eventually as veto objects
            logger.debug("Applying muon preselection")
            muons = self.select_muons(muons, 10., 2.4)
            # then we do the same for electrons
            logger.debug("Applying electron preselection")
            electrons = self.select_electrons(electrons, 10., 2.5, self.meta['Ele_ID'])
            # and finally for taus
            logger.debug("Applying tau preselection")
            taus = self.select_taus(taus, 18., 2.3, self.meta['Tau_ID'])

            # we sort the objects here to ensure the pairs will later be ordered preferring the most isolated particles, if objects have the same isolation then the highest pT one is preferred.
            # To ensure the correct sorting we first sort based on the least important quantity (pT), then after this we sort by the isolation

            # first sort all objects in each event descending in pt
            muons = muons[awkward.argsort(muons.pt, ascending=False)]
            electrons = electrons[awkward.argsort(electrons.pt, ascending=False)]
            taus = taus[awkward.argsort(taus.pt, ascending=False)]

            # then sort all electrons and muon by isolation (smallest isolation first)
            muons = muons[awkward.argsort(muons.pfRelIso04_all, ascending=True)]
            electrons = electrons[awkward.argsort(electrons.miniPFRelIso_all, ascending=True)]

            # also sort taus based on the RAW ID score for vs jet ID, in this case high scores correspond to 'more isolated'
            taus = taus[awkward.argsort(taus[f"raw{self.meta['Tau_ID']}VSjet"], ascending=False)]

            # create map containing pairs for each channel, if (mm, ee, em, et, tt)
            pairs = {}

            logger.debug("Producing ditau pairs")
            # first we make same-type pairs
            if 'mm' in channels:
                mm_pairs = awkward.combinations(muons, 2, fields=["obj_1", "obj_2"])
                pairs['mm'] = mm_pairs
            if 'ee' in channels:
                ee_pairs = awkward.combinations(electrons, 2, fields=["obj_1", "obj_2"])
                pairs['ee'] = ee_pairs
            if 'tt' in channels:
                tt_pairs = awkward.combinations(taus, 2, fields=["obj_1", "obj_2"])
                pairs['tt'] = tt_pairs

            # As preference was given to isolated particles during the sorting, for same-type pairs we need to make sure that obj_1 is always the one with the highest pT:
            for ch in ['ee','mm','tt']:
                if ch not in channels:
                    continue
                # Check if obj_1 has greater pt than obj_2 in each pair
                mask = pairs[ch]["obj_1"].pt > pairs[ch]["obj_2"].pt

                # Switch obj_1 and obj_2 if necessary
                switched_pairs = awkward.with_field(
                    pairs[ch],
                    awkward.where(mask, pairs[ch]["obj_1"], pairs[ch]["obj_2"]),
                    "obj_1"
                )
                switched_pairs = awkward.with_field(
                    switched_pairs,
                    awkward.where(mask, pairs[ch]["obj_2"], pairs[ch]["obj_1"]),
                    "obj_2"
                )
                pairs[ch] = switched_pairs

            # combinations of mixed pairs we follow the usual HTT conventions and lighter leptons are preferred for obj_1
            # Note when using the cartesian function the pairs will be ordered preferentially by the objects passed as the first argument
            # We want to order the pairs prefering muons > electrons > taus so we must pass the arguments in the same order below

            if 'em' in channels:
                em_pairs = awkward.cartesian({"muons": muons,"electrons": electrons})
                em_pairs = awkward.zip({"obj_1": em_pairs["electrons"], "obj_2":em_pairs["muons"]})
                pairs['em'] = em_pairs

            if 'et' in channels:
                et_pairs = awkward.cartesian({"electrons": electrons, "taus": taus})
                et_pairs = awkward.zip({"obj_1": et_pairs["electrons"], "obj_2":et_pairs["taus"]})
                pairs['et'] = et_pairs

            if 'mt' in channels:
                mt_pairs = awkward.cartesian({"muons": muons, "taus": taus})
                mt_pairs = awkward.zip({"obj_1": mt_pairs["muons"], "obj_2":mt_pairs["taus"]})
                pairs['mt'] = mt_pairs

            # get Jet and MET collections
            met = met_dct["nominal"]

            # now loop over all channels, apply common selection requirements and add common quantities for each, then write the outputs
            for ch, p in pairs.items():
                logger.debug(f"Processing {ch} channel pairs")

                logger.debug("Applying channel specific selections")

                # dR cut
                dR_cut = p["obj_1"].delta_r(p["obj_2"]) > 0.5

                # pT and eta cuts
                pt_cut = (p["obj_1"].pt > self.meta['Selections'][ch]['pt_1']) & (p["obj_2"].pt > self.meta['Selections'][ch]['pt_2'])
                eta_cut = (abs(p["obj_1"].eta) < abs(self.meta['Selections'][ch]['eta_1'])) & (abs(p["obj_2"].eta) < abs(self.meta['Selections'][ch]['eta_2']))

                # extra lepton vetos
                lepton_veto_cut = ((self.meta['Selections'][ch]['max_n_electrons'] < 0) | (awkward.num(electrons, axis=1) <= self.meta['Selections'][ch]['max_n_electrons'])) & ((self.meta['Selections'][ch]['max_n_muons'] < 0) | (awkward.num(muons, axis=1) <= self.meta['Selections'][ch]['max_n_muons']))

                all_cuts = dR_cut & pt_cut & eta_cut & lepton_veto_cut

                # apply masks to remove pairs that fail any of the cuts
                p = p[all_cuts]

                # finally we keep only the first pair (which is the highest ranked one)
                p = awkward.firsts(p)

                logger.debug(f"Dropping events without a selected {ch} pair")
                # drop events without a preselected pair
                selection_mask = ~awkward.is_none(p)
                pair = p[selection_mask]

                # Add pair quantities e.g visible-mass, pT etc
                pair = self.add_pair_quantities(pair)

                # Now we pick up the MET and jets, and we apply the same mask that we used to select only our good pairs
                original_met = events[self.meta['MET_Type']][selection_mask]
                original_jets = events.Jet[selection_mask]
                selected_jets = self.select_jets(original_jets,30.,4.7)
                # select b-jet candidates that pass pT and eta cuts for bjets
                selected_bjets = self.select_jets(original_jets,self.meta['BTAG_ID']['pt'],self.meta['BTAG_ID']['eta'])
                # require b-jet candidates to pass cut on btagging discriminator
                selected_bjets = selected_bjets[selected_bjets[self.meta['BTAG_ID']['name']] > self.meta['BTAG_ID']['cut']]
                met_dct = {}
                met_dct["nominal"] = original_met
                jets_dct = {}
                jets_dct["nominal"] = selected_jets
                bjets_dct = {}
                bjets_dct["nominal"] = selected_bjets

                # we can loop over systematics for met and jets from here
                jets = jets_dct["nominal"]
                bjets = bjets_dct["nominal"]
                met = met_dct["nominal"]

                # Add jet quantities here
                # First we filter the jets that are matched in dR to our selected tau candidates
                dr_jet_obj1_cut = delta_r_mask(jets, pair['obj_1'], 0.5)
                dr_jet_obj2_cut = delta_r_mask(jets, pair['obj_2'], 0.5)
                filtered_jets = jets[dr_jet_obj1_cut & dr_jet_obj2_cut]
                # And then we compute and add our jet quantities
                pair = self.add_jet_quantities(pair,filtered_jets)

                # also add number of b-tagged jets
                # as for other jets we filter the bjets that are matched in dR to our selected tau candidates
                dr_bjet_obj1_cut = delta_r_mask(bjets, pair['obj_1'], 0.5)
                dr_bjet_obj2_cut = delta_r_mask(bjets, pair['obj_2'], 0.5)
                filtered_bjets = bjets[dr_bjet_obj1_cut & dr_bjet_obj2_cut]
                # for now we don't use a dedicated function for bjet quantities as we only store n_bjets but this
                # could be added if we eventually want to store more information
                pair["n_bjets"] = awkward.num(filtered_bjets)

                # Add MET quantities e.g MET, transverse masses etc
                pair = self.add_met_quantities(pair,met)

                # Get trigger objects
                trig_objects = events.TrigObj[selection_mask]
                # Apply trigger matching which adds booleons to the array indicating whether the pair legs were matched to particular trigger objects
                if 'Triggers' in self.meta and ch in self.meta['Triggers']:
                    logger.debug("Determining trigger decisions")
                    triggers = self.meta['Triggers'][ch]
                    # loop over all specified triggers and store a booleon for each one indicating which events passed the trigger
                    for trig_name, trig_sels in triggers.items():
                        leg1_selections = trig_sels['leg1'] if 'leg1' in trig_sels else None
                        leg2_selections = trig_sels['leg2'] if 'leg2' in trig_sels else None
                        leg3_selections = trig_sels['leg3'] if 'leg3' in trig_sels else None
                        leg4_selections = trig_sels['leg4'] if 'leg4' in trig_sels else None
                        pair = self.apply_triggers(trig_objects, pair, trig_name, leg1_selections, leg2_selections, leg3_selections, leg4_selections)
                else:
                    logger.debug("Not requiring any triggers as none were specified in the metaconditions")

                # workflow specific processing
                events, process_extra = self.process_extra(events)
                histos_etc.update(process_extra)

                # continue if there is no surviving events
                if len(pair) == 0:
                    logger.debug(f"No surviving {ch} channel events in this run, no output will be written!")
                    continue

                if self.output_location is not None:
                    akarr = ditau_ak_array(self, pair)
                    akarr = self.rename_and_drop(akarr, ch)
                    fname = (
                        events.behavior["__events_factory__"]._partition_key.replace(
                            "/", "_"
                        )
                        + ".parquet"
                    )
                    subdirs = [ch]
                    if "dataset" in events.metadata:
                        subdirs.append(events.metadata["dataset"])
                    subdirs.append(variation)
                    dump_ak_array(self, akarr, fname, self.output_location, metadata, subdirs)

        return histos_etc

    def postprocess(self, accumulant: Dict[Any, Any]) -> Any:
        raise NotImplementedError


class HggBaseProcessor(processor.ProcessorABC):  # type: ignore
    def __init__(
        self,
        metaconditions: Dict[str, Any],
        systematics: Optional[Dict[str, List[str]]],
        corrections: Optional[Dict[str, List[str]]],
        apply_trigger: bool,
        output_location: Optional[str],
        taggers: Optional[List[Any]],
        trigger_group: str,
        analysis: str,
        skipCQR: bool,
        skipJetVetoMap: bool,
        year: Optional[Dict[str, List[str]]],
        doDeco: bool,
    ) -> None:
        self.meta = metaconditions
        self.systematics = systematics if systematics is not None else {}
        self.corrections = corrections if corrections is not None else {}
        self.apply_trigger = apply_trigger
        self.output_location = output_location
        self.trigger_group = trigger_group
        self.analysis = analysis
        self.skipCQR = skipCQR
        self.skipJetVetoMap = skipJetVetoMap
        self.year = year if year is not None else {}
        self.doDeco = doDeco

        # muon selection cuts
        self.muon_pt_threshold = 10
        self.muon_max_eta = 2.4
        self.mu_iso_wp = "medium"
        self.global_muon = False

        # electron selection cuts
        self.electron_pt_threshold = 15
        self.electron_max_eta = 2.5
        self.el_iso_wp = "WP80"

        # jet selection cuts
        self.jet_dipho_min_dr = 0.4
        self.jet_pho_min_dr = 0.4
        self.jet_ele_min_dr = 0.4
        self.jet_muo_min_dr = 0.4
        self.jet_pt_threshold = 20
        self.jet_max_eta = 4.7

        self.clean_jet_dipho = True
        self.clean_jet_pho = True
        self.clean_jet_ele = False
        self.clean_jet_muo = False

        # diphoton preselection cuts
        self.min_pt_photon = 25.0
        self.min_pt_lead_photon = 35.0
        self.min_mvaid = -0.9
        self.max_sc_eta = 2.5
        self.gap_barrel_eta = 1.4442
        self.gap_endcap_eta = 1.566
        self.max_hovere = 0.08
        self.min_full5x5_r9 = 0.8
        self.max_chad_iso = 20.0
        self.max_chad_rel_iso = 0.3

        self.min_full5x5_r9_EB_high_r9 = 0.85
        self.min_full5x5_r9_EE_high_r9 = 0.9
        self.min_full5x5_r9_EB_low_r9 = 0.5
        self.min_full5x5_r9_EE_low_r9 = 0.8
        self.max_trkSumPtHollowConeDR03_EB_low_r9 = (
            6.0  # for v11, we cut on Photon_pfChargedIsoPFPV
        )
        self.max_trkSumPtHollowConeDR03_EE_low_r9 = 6.0  # Leaving the names of the preselection cut variables the same to change as little as possible
        self.max_sieie_EB_low_r9 = 0.015
        self.max_sieie_EE_low_r9 = 0.035
        self.max_pho_iso_EB_low_r9 = 4.0
        self.max_pho_iso_EE_low_r9 = 4.0

        self.eta_rho_corr = 1.5
        self.low_eta_rho_corr = 0.16544
        self.high_eta_rho_corr = 0.13212
        self.e_veto = 0.5

        logger.debug(f"Setting up processor with metaconditions: {self.meta}")

        self.taggers = []
        if taggers is not None:
            self.taggers = taggers
            self.taggers.sort(key=lambda x: x.priority)

        self.prefixes = {"pho_lead": "lead", "pho_sublead": "sublead"}

        if not self.doDeco:
            logger.info("Skipping Mass resolution decorrelation as required")
        else:
            logger.info("Performing Mass resolution decorrelation as required")

        # build the chained quantile regressions
        if not self.skipCQR:
            try:
                self.chained_quantile: Optional[
                    ChainedQuantileRegression
                ] = ChainedQuantileRegression(**self.meta["PhoIdInputCorrections"])
            except Exception as e:
                warnings.warn(f"Could not instantiate ChainedQuantileRegression: {e}")
                self.chained_quantile = None
        else:
            logger.info("Skipping CQR as required")
            self.chained_quantile = None

        # initialize photonid_mva
        photon_id_mva_dir = os.path.dirname(photon_id_mva_weights.__file__)
        try:
            logger.debug(
                f"Looking for {self.meta['flashggPhotons']['photonIdMVAweightfile_EB']} in {photon_id_mva_dir}"
            )
            self.photonid_mva_EB = load_photonid_mva(
                os.path.join(
                    photon_id_mva_dir,
                    self.meta["flashggPhotons"]["photonIdMVAweightfile_EB"],
                )
            )
            self.photonid_mva_EE = load_photonid_mva(
                os.path.join(
                    photon_id_mva_dir,
                    self.meta["flashggPhotons"]["photonIdMVAweightfile_EE"],
                )
            )
        except Exception as e:
            warnings.warn(f"Could not instantiate PhotonID MVA on the fly: {e}")
            self.photonid_mva_EB = None
            self.photonid_mva_EE = None

        # initialize diphoton mva
        diphoton_weights_dir = os.path.dirname(diphoton_mva_dir.__file__)
        logger.debug(
            f"Base path to look for IDMVA weight files: {diphoton_weights_dir}"
        )

        try:
            self.diphoton_mva = load_bdt(
                os.path.join(
                    diphoton_weights_dir, self.meta["flashggDiPhotonMVA"]["weightFile"]
                )
            )
        except Exception as e:
            warnings.warn(f"Could not instantiate diphoton MVA: {e}")
            self.diphoton_mva = None

    def process_extra(self, events: awkward.Array) -> awkward.Array:
        raise NotImplementedError

    def apply_filters_and_triggers(self, events: awkward.Array) -> awkward.Array:
        # met filters
        met_filters = self.meta["flashggMetFilters"][self.data_kind]
        filtered = functools.reduce(
            operator.and_,
            (events.Flag[metfilter.split("_")[-1]] for metfilter in met_filters),
        )

        triggered = awkward.ones_like(filtered)
        if self.apply_trigger:
            trigger_names = []
            triggers = self.meta["TriggerPaths"][self.trigger_group][self.analysis]
            hlt = events.HLT
            for trigger in triggers:
                actual_trigger = trigger.replace("HLT_", "").replace("*", "")
                for field in hlt.fields:
                    if field.startswith(actual_trigger):
                        trigger_names.append(field)
            triggered = functools.reduce(
                operator.or_, (hlt[trigger_name] for trigger_name in trigger_names)
            )

        return events[filtered & triggered]

    def process(self, events: awkward.Array) -> Dict[Any, Any]:
        dataset_name = events.metadata["dataset"]

        # data or monte carlo?
        self.data_kind = "mc" if hasattr(events, "GenPart") else "data"

        # here we start recording possible coffea accumulators
        # most likely histograms, could be counters, arrays, ...
        histos_etc = {}
        histos_etc[dataset_name] = {}
        if self.data_kind == "mc":
            histos_etc[dataset_name]["nTot"] = int(
                awkward.num(events.genWeight, axis=0)
            )
            histos_etc[dataset_name]["nPos"] = int(awkward.sum(events.genWeight > 0))
            histos_etc[dataset_name]["nNeg"] = int(awkward.sum(events.genWeight < 0))
            histos_etc[dataset_name]["nEff"] = int(
                histos_etc[dataset_name]["nPos"] - histos_etc[dataset_name]["nNeg"]
            )
            histos_etc[dataset_name]["genWeightSum"] = float(
                awkward.sum(events.genWeight)
            )
        else:
            histos_etc[dataset_name]["nTot"] = int(len(events))
            histos_etc[dataset_name]["nPos"] = int(histos_etc[dataset_name]["nTot"])
            histos_etc[dataset_name]["nNeg"] = int(0)
            histos_etc[dataset_name]["nEff"] = int(histos_etc[dataset_name]["nTot"])
            histos_etc[dataset_name]["genWeightSum"] = float(len(events))

        # lumi mask
        if self.data_kind == "data":
            try:
                lumimask = select_lumis(self.year[dataset_name][0], events, logger)
                events = events[lumimask]
            except:
                logger.info(
                    f"[ lumimask ] Skip now! Unable to find year info of {dataset_name}"
                )
        # apply jetvetomap
        if not self.skipJetVetoMap:
            events = jetvetomap(
                events, logger, dataset_name, year=self.year[dataset_name][0]
            )
        # metadata array to append to higgsdna output
        metadata = {}

        if self.data_kind == "mc":
            # Add sum of gen weights before selection for normalisation in postprocessing
            metadata["sum_genw_presel"] = str(awkward.sum(events.genWeight))
        else:
            metadata["sum_genw_presel"] = "Data"

        # apply filters and triggers
        events = self.apply_filters_and_triggers(events)

        # we need ScEta for corrections and systematics, which is not present in NanoAODv11 but can be calculated using PV
        events.Photon = add_photon_SC_eta(events.Photon, events.PV)

        # add veto EE leak branch for photons, could also be used for electrons
        if (
            self.year[dataset_name][0] == "2022EE"
            or self.year[dataset_name][0] == "2022postEE"
        ):
            events.Photon = veto_EEleak_flag(self, events.Photon)

        # read which systematics and corrections to process
        try:
            correction_names = self.corrections[dataset_name]
        except KeyError:
            correction_names = []
        try:
            systematic_names = self.systematics[dataset_name]
        except KeyError:
            systematic_names = []

        # object corrections:
        for correction_name in correction_names:
            if correction_name in available_object_corrections.keys():
                logger.info(
                    f"Applying correction {correction_name} to dataset {dataset_name}"
                )
                varying_function = available_object_corrections[correction_name]
                events = varying_function(events=events)
            elif correction_name in available_weight_corrections:
                # event weight corrections will be applied after photon preselection / application of further taggers
                continue
            else:
                # may want to throw an error instead, needs to be discussed
                warnings.warn(f"Could not process correction {correction_name}.")
                continue

        original_photons = events.Photon
        original_jets = events.Jet
        # systematic object variations
        for systematic_name in systematic_names:
            if systematic_name in available_object_systematics.keys():
                systematic_dct = available_object_systematics[systematic_name]
                if systematic_dct["object"] == "Photon":
                    logger.info(
                        f"Adding systematic {systematic_name} to photons collection of dataset {dataset_name}"
                    )
                    original_photons.add_systematic(
                        # passing the arguments here explicitly since I want to pass the events to the varying function. If there is a more elegant / flexible way, just change it!
                        name=systematic_name,
                        kind=systematic_dct["args"]["kind"],
                        what=systematic_dct["args"]["what"],
                        varying_function=functools.partial(
                            systematic_dct["args"]["varying_function"],
                            events=events,
                            year=self.year[dataset_name][0],
                        )
                        # name=systematic_name, **systematic_dct["args"]
                    )
                elif systematic_dct["object"] == "Jet":
                    logger.info(
                        f"Adding systematic {systematic_name} to jets collection of dataset {dataset_name}"
                    )
                    original_jets.add_systematic(
                        # passing the arguments here explicitly since I want to pass the events to the varying function. If there is a more elegant / flexible way, just change it!
                        name=systematic_name,
                        kind=systematic_dct["args"]["kind"],
                        what=systematic_dct["args"]["what"],
                        varying_function=functools.partial(
                            systematic_dct["args"]["varying_function"], events=events
                        )
                        # name=systematic_name, **systematic_dct["args"]
                    )
                # to be implemented for other objects here
            elif systematic_name in available_weight_systematics:
                # event weight systematics will be applied after photon preselection / application of further taggers
                continue
            else:
                # may want to throw an error instead, needs to be discussed
                warnings.warn(
                    f"Could not process systematic variation {systematic_name}."
                )
                continue

        photons_dct = {}
        photons_dct["nominal"] = original_photons
        logger.debug(original_photons.systematics.fields)
        for systematic in original_photons.systematics.fields:
            for variation in original_photons.systematics[systematic].fields:
                # deepcopy to allow for independent calculations on photon variables with CQR
                photons_dct[f"{systematic}_{variation}"] = deepcopy(
                    original_photons.systematics[systematic][variation]
                )

        jets_dct = {}
        jets_dct["nominal"] = original_jets
        logger.debug(original_jets.systematics.fields)
        for systematic in original_jets.systematics.fields:
            for variation in original_jets.systematics[systematic].fields:
                # deepcopy to allow for independent calculations on photon variables with CQR
                jets_dct[f"{systematic}_{variation}"] = original_jets.systematics[
                    systematic
                ][variation]

        for variation, photons in photons_dct.items():
            for jet_variation, Jets in jets_dct.items():
                # make sure no duplicate executions
                if variation == "nominal" or jet_variation == "nominal":
                    if variation != "nominal" and jet_variation != "nominal":
                        continue
                    do_variation = "nominal"
                    if not (variation == "nominal" and jet_variation == "nominal"):
                        do_variation = (
                            variation if variation != "nominal" else jet_variation
                        )
                    logger.debug("Variation: {}".format(do_variation))
                    if self.chained_quantile is not None:
                        photons = self.chained_quantile.apply(photons, events)
                    # recompute photonid_mva on the fly
                    if self.photonid_mva_EB and self.photonid_mva_EE:
                        photons = self.add_photonid_mva(photons, events)

                    # photon preselection
                    photons = photon_preselection(self, photons, events)
                    # sort photons in each event descending in pt
                    # make descending-pt combinations of photons
                    photons = photons[awkward.argsort(photons.pt, ascending=False)]
                    photons["charge"] = awkward.zeros_like(
                        photons.pt
                    )  # added this because charge is not a property of photons in nanoAOD v11. We just assume every photon has charge zero...
                    diphotons = awkward.combinations(
                        photons, 2, fields=["pho_lead", "pho_sublead"]
                    )
                    # the remaining cut is to select the leading photons
                    # the previous sort assures the order
                    diphotons = diphotons[
                        diphotons["pho_lead"].pt > self.min_pt_lead_photon
                    ]

                    # now turn the diphotons into candidates with four momenta and such
                    diphoton_4mom = diphotons["pho_lead"] + diphotons["pho_sublead"]
                    diphotons["pt"] = diphoton_4mom.pt
                    diphotons["eta"] = diphoton_4mom.eta
                    diphotons["phi"] = diphoton_4mom.phi
                    diphotons["mass"] = diphoton_4mom.mass
                    diphotons["charge"] = diphoton_4mom.charge
                    diphotons = awkward.with_name(diphotons, "PtEtaPhiMCandidate")

                    # sort diphotons by pT
                    diphotons = diphotons[
                        awkward.argsort(diphotons.pt, ascending=False)
                    ]

                    # baseline modifications to diphotons
                    if self.diphoton_mva is not None:
                        diphotons = self.add_diphoton_mva(diphotons, events)

                    # workflow specific processing
                    events, process_extra = self.process_extra(events)
                    histos_etc.update(process_extra)

                    # jet_variables
                    jets = awkward.zip(
                        {
                            "pt": Jets.pt,
                            "eta": Jets.eta,
                            "phi": Jets.phi,
                            "mass": Jets.mass,
                            "charge": awkward.zeros_like(
                                Jets.pt
                            ),  # added this because jet charge is not a property of photons in nanoAOD v11. We just need the charge to build jet collection.
                            "hFlav": Jets.hadronFlavour
                            if self.data_kind == "mc"
                            else awkward.zeros_like(Jets.pt),
                            "btagDeepFlav_B": Jets.btagDeepFlavB,
                            "btagDeepFlav_CvB": Jets.btagDeepFlavCvB,
                            "btagDeepFlav_CvL": Jets.btagDeepFlavCvL,
                            "btagDeepFlav_QG": Jets.btagDeepFlavQG,
                        }
                    )
                    jets = awkward.with_name(jets, "PtEtaPhiMCandidate")

                    electrons = awkward.zip(
                        {
                            "pt": events.Electron.pt,
                            "eta": events.Electron.eta,
                            "phi": events.Electron.phi,
                            "mass": events.Electron.mass,
                            "charge": events.Electron.charge,
                            "mvaIso_WP90": events.Electron.mvaIso_WP90,
                            "mvaIso_WP80": events.Electron.mvaIso_WP80,
                        }
                    )
                    electrons = awkward.with_name(electrons, "PtEtaPhiMCandidate")

                    muons = awkward.zip(
                        {
                            "pt": events.Muon.pt,
                            "eta": events.Muon.eta,
                            "phi": events.Muon.phi,
                            "mass": events.Muon.mass,
                            "charge": events.Muon.charge,
                            "tightId": events.Muon.tightId,
                            "mediumId": events.Muon.mediumId,
                            "looseId": events.Muon.looseId,
                            "isGlobal": events.Muon.isGlobal,
                        }
                    )
                    muons = awkward.with_name(muons, "PtEtaPhiMCandidate")

                    # lepton cleaning
                    sel_electrons = electrons[
                        select_electrons(self, electrons, diphotons)
                    ]
                    sel_muons = muons[select_muons(self, muons, diphotons)]

                    # jet selection and pt ordering
                    jets = jets[
                        select_jets(self, jets, diphotons, sel_muons, sel_electrons)
                    ]
                    jets = jets[awkward.argsort(jets.pt, ascending=False)]

                    # adding selected jets to events to be used in ctagging SF calculation
                    events["sel_jets"] = jets
                    n_jets = awkward.num(jets)

                    first_jet_pt = choose_jet(jets.pt, 0, -999.0)
                    first_jet_eta = choose_jet(jets.eta, 0, -999.0)
                    first_jet_phi = choose_jet(jets.phi, 0, -999.0)
                    first_jet_mass = choose_jet(jets.mass, 0, -999.0)
                    first_jet_charge = choose_jet(jets.charge, 0, -999.0)

                    second_jet_pt = choose_jet(jets.pt, 1, -999.0)
                    second_jet_eta = choose_jet(jets.eta, 1, -999.0)
                    second_jet_phi = choose_jet(jets.phi, 1, -999.0)
                    second_jet_mass = choose_jet(jets.mass, 1, -999.0)
                    second_jet_charge = choose_jet(jets.charge, 1, -999.0)

                    diphotons["first_jet_pt"] = first_jet_pt
                    diphotons["first_jet_eta"] = first_jet_eta
                    diphotons["first_jet_phi"] = first_jet_phi
                    diphotons["first_jet_mass"] = first_jet_mass
                    diphotons["first_jet_charge"] = first_jet_charge

                    diphotons["second_jet_pt"] = second_jet_pt
                    diphotons["second_jet_eta"] = second_jet_eta
                    diphotons["second_jet_phi"] = second_jet_phi
                    diphotons["second_jet_mass"] = second_jet_mass
                    diphotons["second_jet_charge"] = second_jet_charge

                    diphotons["n_jets"] = n_jets

                    # run taggers on the events list with added diphotons
                    # the shape here is ensured to be broadcastable
                    for tagger in self.taggers:
                        (
                            diphotons["_".join([tagger.name, str(tagger.priority)])],
                            tagger_extra,
                        ) = tagger(
                            events, diphotons
                        )  # creates new column in diphotons - tagger priority, or 0, also return list of histrograms here?
                        histos_etc.update(tagger_extra)

                    # if there are taggers to run, arbitrate by them first
                    # Deal with order of tagger priorities
                    # Turn from diphoton jagged array to whether or not an event was selected
                    if len(self.taggers):
                        counts = awkward.num(diphotons.pt, axis=1)
                        flat_tags = numpy.stack(
                            (
                                awkward.flatten(
                                    diphotons[
                                        "_".join([tagger.name, str(tagger.priority)])
                                    ]
                                )
                                for tagger in self.taggers
                            ),
                            axis=1,
                        )
                        tags = awkward.from_regular(
                            awkward.unflatten(flat_tags, counts), axis=2
                        )
                        winner = awkward.min(tags[tags != 0], axis=2)
                        diphotons["best_tag"] = winner

                        # lowest priority is most important (ascending sort)
                        # leave in order of diphoton pT in case of ties (stable sort)
                        sorted = awkward.argsort(diphotons.best_tag, stable=True)
                        diphotons = diphotons[sorted]

                    diphotons = awkward.firsts(diphotons)
                    # set diphotons as part of the event record
                    events[f"diphotons_{do_variation}"] = diphotons
                    # annotate diphotons with event information
                    diphotons["event"] = events.event
                    diphotons["lumi"] = events.luminosityBlock
                    diphotons["run"] = events.run
                    # nPV just for validation of pileup reweighting
                    diphotons["nPV"] = events.PV.npvs
                    diphotons["fixedGridRhoAll"] = events.Rho.fixedGridRhoAll
                    # annotate diphotons with dZ information (difference between z position of GenVtx and PV) as required by flashggfinalfits
                    if self.data_kind == "mc":
                        diphotons["genWeight"] = events.genWeight
                        diphotons["dZ"] = events.GenVtx.z - events.PV.z
                        # Necessary for differential xsec measurements in final fits ("truth" variables)
                        diphotons["HTXS_Higgs_pt"] = events.HTXS.Higgs_pt
                        diphotons["HTXS_Higgs_y"] = events.HTXS.Higgs_y
                        diphotons["HTXS_njets30"] = events.HTXS.njets30  # Need to clarify if this variable is suitable, does it fulfill abs(eta_j) < 2.5? Probably not
                        # Preparation for HTXS measurements later, start with stage 0 to disentangle VH into WH and ZH for final fits
                        diphotons["HTXS_stage_0"] = events.HTXS.stage_0
                    # Fill zeros for data because there is no GenVtx for data, obviously
                    else:
                        diphotons["dZ"] = awkward.zeros_like(events.PV.z)

                    ### Add mass resolution uncertainty
                    # Note that pt*cosh(eta) is equal to the energy of a four vector
                    # Note that you need to call it slightly different than in the output of HiggsDNA as pho_lead -> lead is only done in dumping utils
                    diphotons["sigma_m_over_m"] = 0.5 * numpy.sqrt(
                        (
                            diphotons["pho_lead"].energyErr
                            / (
                                diphotons["pho_lead"].pt
                                * numpy.cosh(diphotons["pho_lead"].eta)
                            )
                        )
                        ** 2
                        + (
                            diphotons["pho_sublead"].energyErr
                            / (
                                diphotons["pho_sublead"].pt
                                * numpy.cosh(diphotons["pho_sublead"].eta)
                            )
                        )
                        ** 2
                    )

                    # drop events without a preselected diphoton candidate
                    # drop events without a tag, if there are tags
                    if len(self.taggers):
                        selection_mask = ~(
                            awkward.is_none(diphotons)
                            | awkward.is_none(diphotons.best_tag)
                        )
                        diphotons = diphotons[selection_mask]
                    else:
                        selection_mask = ~awkward.is_none(diphotons)
                        diphotons = diphotons[selection_mask]

                    # return if there is no surviving events
                    if len(diphotons) == 0:
                        logger.debug("No surviving events in this run, return now!")
                        return histos_etc
                    if self.data_kind == "mc":
                        # initiate Weight container here, after selection, since event selection cannot easily be applied to weight container afterwards
                        event_weights = Weights(size=len(events[selection_mask]))

                        # corrections to event weights:
                        for correction_name in correction_names:
                            if correction_name in available_weight_corrections:
                                logger.info(
                                    f"Adding correction {correction_name} to weight collection of dataset {dataset_name}"
                                )
                                varying_function = available_weight_corrections[
                                    correction_name
                                ]
                                event_weights = varying_function(
                                    events=events[selection_mask],
                                    photons=events[f"diphotons_{do_variation}"][
                                        selection_mask
                                    ],
                                    weights=event_weights,
                                    dataset_name=dataset_name,
                                    year=self.year[dataset_name][0],
                                )

                        # systematic variations of event weights go to nominal output dataframe:
                        if do_variation == "nominal":
                            for systematic_name in systematic_names:
                                if systematic_name in available_weight_systematics:
                                    logger.info(
                                        f"Adding systematic {systematic_name} to weight collection of dataset {dataset_name}"
                                    )
                                    if systematic_name == "LHEScale":
                                        if hasattr(events, "LHEScaleWeight"):
                                            diphotons["nLHEScaleWeight"] = awkward.num(
                                                events.LHEScaleWeight[selection_mask],
                                                axis=1,
                                            )
                                            diphotons[
                                                "LHEScaleWeight"
                                            ] = events.LHEScaleWeight[selection_mask]
                                        else:
                                            logger.info(
                                                f"No {systematic_name} Weights in dataset {dataset_name}"
                                            )
                                    elif systematic_name == "LHEPdf":
                                        if hasattr(events, "LHEPdfWeight"):
                                            # two AlphaS weights are removed
                                            diphotons["nLHEPdfWeight"] = (
                                                awkward.num(
                                                    events.LHEPdfWeight[selection_mask],
                                                    axis=1,
                                                )
                                                - 2
                                            )
                                            diphotons[
                                                "LHEPdfWeight"
                                            ] = events.LHEPdfWeight[selection_mask][
                                                :, :-2
                                            ]
                                        else:
                                            logger.info(
                                                f"No {systematic_name} Weights in dataset {dataset_name}"
                                            )
                                    else:
                                        varying_function = available_weight_systematics[
                                            systematic_name
                                        ]
                                        event_weights = varying_function(
                                            events=events[selection_mask],
                                            photons=events[f"diphotons_{do_variation}"][
                                                selection_mask
                                            ],
                                            weights=event_weights,
                                            dataset_name=dataset_name,
                                            year=self.year[dataset_name][0],
                                        )

                        diphotons["weight_central"] = event_weights.weight()
                        # Store variations with respect to central weight
                        if do_variation == "nominal":
                            if len(event_weights.variations):
                                logger.info(
                                    "Adding systematic weight variations to nominal output file."
                                )
                            for modifier in event_weights.variations:
                                diphotons["weight_" + modifier] = event_weights.weight(
                                    modifier=modifier
                                )

                        # Multiply weight by genWeight for normalisation in post-processing chain
                        event_weights._weight = (
                            events["genWeight"][selection_mask]
                            * diphotons["weight_central"]
                        )
                        diphotons["weight"] = event_weights.weight()

                    # Add weight variables (=1) for data for consistent datasets
                    else:
                        diphotons["weight_central"] = awkward.ones_like(
                            diphotons["event"]
                        )
                        diphotons["weight"] = awkward.ones_like(diphotons["event"])

                    # Decorrelating the mass resolution - Still need to supress the decorrelator noises
                    if self.doDeco:
                        diphotons["sigma_m_over_m_decorr"] = decorrelate_mass_resolution(diphotons)

                    if self.output_location is not None:
                        # df = diphoton_list_to_pandas(self, diphotons)
                        akarr = diphoton_ak_array(self, diphotons)

                        # Remove fixedGridRhoAll from photons to avoid having event-level info per photon
                        akarr = akarr[
                            [
                                field
                                for field in akarr.fields
                                if "lead_fixedGridRhoAll" not in field
                            ]
                        ]

                        fname = (
                            events.behavior[
                                "__events_factory__"
                            ]._partition_key.replace("/", "_")
                            + ".parquet"
                        )
                        subdirs = []
                        if "dataset" in events.metadata:
                            subdirs.append(events.metadata["dataset"])
                        subdirs.append(do_variation)
                        # dump_pandas(self, df, fname, self.output_location, subdirs)
                        dump_ak_array(
                            self, akarr, fname, self.output_location, metadata, subdirs
                        )

        return histos_etc

    def postprocess(self, accumulant: Dict[Any, Any]) -> Any:
        raise NotImplementedError

    def add_diphoton_mva(
        self, diphotons: awkward.Array, events: awkward.Array
    ) -> awkward.Array:
        return calculate_diphoton_mva(
            (self.diphoton_mva, self.meta["flashggDiPhotonMVA"]["inputs"]),
            diphotons,
            events,
        )

    def add_photonid_mva(
        self, photons: awkward.Array, events: awkward.Array
    ) -> awkward.Array:
        photons["fixedGridRhoAll"] = events.Rho.fixedGridRhoAll * awkward.ones_like(
            photons.pt
        )
        counts = awkward.num(photons, axis=-1)
        photons = awkward.flatten(photons)
        isEB = awkward.to_numpy(numpy.abs(photons.eta) < 1.5)
        mva_EB = calculate_photonid_mva(
            (self.photonid_mva_EB, self.meta["flashggPhotons"]["inputs_EB"]), photons
        )
        mva_EE = calculate_photonid_mva(
            (self.photonid_mva_EE, self.meta["flashggPhotons"]["inputs_EE"]), photons
        )
        mva = awkward.where(isEB, mva_EB, mva_EE)
        photons["mvaID"] = mva

        return awkward.unflatten(photons, counts)
