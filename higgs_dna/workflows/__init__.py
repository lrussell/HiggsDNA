from higgs_dna.workflows.dystudies import (
    DYStudiesProcessor,
    TagAndProbeProcessor,
)
from higgs_dna.workflows.taggers import taggers
from higgs_dna.workflows.ditau import DiTauProcessor

workflows = {}

workflows["dystudies"] = DYStudiesProcessor
workflows["tagandprobe"] = TagAndProbeProcessor
workflows["ditau"] = DiTauProcessor

__all__ = ["workflows", "taggers", "DYStudiesProcessor"]
